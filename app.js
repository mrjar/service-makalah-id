const express = require('express');
const app = express();

// Settings
app.set('port', process.env.PORT || 3636);

// Middlewares
app.use(express.json());

// Routes
app.use(require('./src/routes/service_member'));
app.use(require('./src/routes/service_product'));
app.use(require('./src/routes/service_ads'));
app.use(require('./src/not_found'));

// Starting the server
app.listen(app.get('port'), () => {
    console.log(`Service NodeJs Express with MySQL is running on port  ${app.get('port')}`);
});
var express = require('express');
const router = express.Router();
var path = require('path');

router.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

// Default Route
router.get('/', (reg, res) => {
    return res.sendFile(path.join(__dirname + '/not_found.html'));
    
});

module.exports = router;
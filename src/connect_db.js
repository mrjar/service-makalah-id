const mysql = require('mysql');

const mysqlConnection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'nodejs',
    connectTimeout: 10000,
    waitForConnections: true,
    acquireTimeout: 10000,
    debug: false,
    multipleStatements: true
});

mysqlConnection.connect(function (err) {
    if (err) {
        console.error('Hallo JEFFRY AZHARI ROSMAN, kamu gagal konek ke DB. pesan Error:', err);
        return;
    } else {
        console.log('Hallo JEFFRY AZHARI ROSMAN, kamu berhasil konek ke DB');
    }
});

module.exports = mysqlConnection;

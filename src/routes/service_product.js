var express = require('express');
var path = require('path');
//bodyParser merubah data Mysql ke JSON
var bodyParser = require('body-parser');
const router = express.Router();
//import file connection DB
const mysqlConnection = require('../connect_db.js');
const image2base64 = require('image-to-base64');
const pdf2base64 = require('pdf-to-base64');

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({
    extended: true
}));
router.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});


// image2base64("src/file-cover-pdf/surat-riser-1.png") // you can also to use url
// pdf2base64("src/file-pdf/surat-riser.pdf") // you can also to use url
//     .then(
//         (response) => {
//             console.log(response); //cGF0aC90by9maWxlLmpwZw==
//         }
//     )
//     .catch(
//         (error) => {
//             console.log(error); //Exepection error....
//         }
//     );


router.post('/makalah/new', function (req, res, next) {
    var kategori;
    var offset;
    if (req.body.data.kategori == undefined) {
        kategori = ""
    } else {
        kategori = "AND kategori='" + req.body.data.kategori + "' ";
    }
    if (req.body.data.offset == undefined) {
        offset = 0;
    } else {
        offset = "OFFSET " + req.body.data.offset;
    }
    mysqlConnection.query(`SELECT * FROM makalah WHERE judul LIKE '%${req.body.data.judul}%' ${kategori}ORDER BY makalah.id_makalah DESC LIMIT 10 ${offset}`, function (error, results, fields) {
        if (error) throw error;

        if (results.length == 0) {
            return res.send({ status: 200, data: [], message: 'List data kosong' });
        } else {
            onload(results);
        }
        async function onload(results) {
            var data = [];
            var string = JSON.stringify(results);
            var json = JSON.parse(string);
            for (var a = 0; a < json.length; a++) {
                var file_base64 = json[a].file_base64;
                var cover_base64 = json[a].cover_base64;
                var id_makalah = json[a].id_makalah;
                var judul = json[a].judul;
                var referensi = json[a].referensi;
                var kategori_referensi = json[a].kategori_referensi;
                var deskripsi = json[a].deskripsi;
                var kategori = json[a].kategori;
                var jumlah_baca = json[a].jumlah_baca;
                await image2base64("src/file-cover-pdf/" + cover_base64).then((responsePng) => {
                    var prosesData = {
                        "id_makalah": id_makalah,
                        "judul": judul,
                        "referensi": referensi,
                        "kategori_referensi": kategori_referensi,
                        "deskripsi": deskripsi,
                        "kategori": kategori,
                        "jumlah_baca": jumlah_baca,
                        "cover_base64": "data:image/png;base64," + responsePng
                    };
                    data.push(prosesData);
                }).catch((error) => {
                    console.log(error); //Exepection error....
                    return res.send({ status: 404, data: null, message: 'File Error' });
                });
            }
            return res.send({ status: 200, data: data, message: 'List data', length: data.length });
        }
    });
});

router.post('/makalah/populer', function (req, res, next) {
    var kategori;
    var offset;
    if (req.body.data.kategori == undefined) {
        kategori = ""
    } else {
        kategori = "AND kategori='" + req.body.data.kategori + "' ";
    }
    if (req.body.data.offset == undefined) {
        offset = 0;
    } else {
        offset = "OFFSET " + req.body.data.offset;
    }
    mysqlConnection.query(`SELECT * FROM makalah WHERE judul LIKE '%${req.body.data.judul}%' ${kategori}ORDER BY makalah.jumlah_baca DESC LIMIT 10 ${offset}`, function (error, results, fields) {
        if (error) throw error;

        if (results.length == 0) {
            return res.send({ status: 200, data: [], message: 'List data kosong' });
        } else {
            onload(results);
        }
        async function onload(results) {
            var data = [];
            var string = JSON.stringify(results);
            var json = JSON.parse(string);
            for (var a = 0; a < json.length; a++) {
                var file_base64 = json[a].file_base64;
                var cover_base64 = json[a].cover_base64;
                var id_makalah = json[a].id_makalah;
                var judul = json[a].judul;
                var referensi = json[a].referensi;
                var kategori_referensi = json[a].kategori_referensi;
                var deskripsi = json[a].deskripsi;
                var kategori = json[a].kategori;
                var jumlah_baca = json[a].jumlah_baca;
                await image2base64("src/file-cover-pdf/" + cover_base64).then((responsePng) => {
                    var prosesData = {
                        "id_makalah": id_makalah,
                        "judul": judul,
                        "referensi": referensi,
                        "kategori_referensi": kategori_referensi,
                        "deskripsi": deskripsi,
                        "kategori": kategori,
                        "jumlah_baca": jumlah_baca,
                        "cover_base64": "data:image/png;base64," + responsePng
                    };
                    data.push(prosesData);
                }).catch((error) => {
                    console.log(error); //Exepection error....
                    return res.send({ status: 404, data: null, message: 'File Error' });
                });
            }
            return res.send({ status: 200, data: data, message: 'List data', length: data.length });
        }
    });
});

router.post('/makalah/openId', function (req, res, next) {

    mysqlConnection.query(`SELECT * FROM makalah WHERE id_makalah = '${req.body.data.idMakalah}'`, function (error, results, fields) {
        if (error) throw error;

        if (results.length == 0) {
            return res.send({ status: 200, data: [], message: 'List data kosong' });
        } else {
            onload(results);
        }
        var data = [];
        async function onload(results) {
            var string = JSON.stringify(results);
            var json = JSON.parse(string);
            for (var a = 0; a < json.length; a++) {
                var file_base64 = json[a].file_base64;
                var cover_base64 = json[a].cover_base64;
                var id_makalah = json[a].id_makalah;
                var judul = json[a].judul;
                var referensi = json[a].referensi;
                var kategori_referensi = json[a].kategori_referensi;
                var deskripsi = json[a].deskripsi;
                var kategori = json[a].kategori;
                var jumlah_baca = parseInt(json[a].jumlah_baca) + 1;

                await pdf2base64("src/file-pdf/" + file_base64).then((responsePdf) => {
                    var prosesData = {
                        "id_makalah": id_makalah,
                        "judul": judul,
                        "referensi": referensi,
                        "kategori_referensi": kategori_referensi,
                        "deskripsi": deskripsi,
                        "kategori": kategori,
                        "jumlah_baca": jumlah_baca,
                        "base64Pdf": responsePdf,
                        "file_base64": "data:application/pdf;base64," + responsePdf
                    };
                    data.push(prosesData);
                    onloadInsert(jumlah_baca);
                }).catch((error) => {
                    console.log(error); //Exepection error....
                    return res.send({ status: 404, data: null, message: 'File Error' });
                });
            }
        }
        async function onloadInsert(jumlah_baca) {
            await mysqlConnection.query(`UPDATE makalah SET jumlah_baca='${jumlah_baca}' WHERE id_makalah='${req.body.data.idMakalah}'`, function (errorInsert, resultsInsert, fieldsInsert) {
                if (errorInsert) throw errorInsert;
                return res.send({ status: 200, data: data, message: 'List data', length: data.length });
            })

        }
    });
});

// Menampilkan data produk detail
// router.get('/pruduct', function (req, res, next) {

//     mysqlConnection.query('SELECT * FROM makalah', function (error, results, fields) {
//         if (error) throw error;
//         return res.send({ status: 200, data: results, message: 'List data' });
//     });
// });

module.exports = router;
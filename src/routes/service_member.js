var express = require('express');
var path = require('path');
//bodyParser merubah data Mysql ke JSON
var bodyParser = require('body-parser');
const router = express.Router();
//import file connection DB
const mysqlConnection = require('../connect_db.js');
//import email
var nodemailer = require('nodemailer');
//token
var jwt = require('jsonwebtoken');


const jwtKey = 'top_secret_key';
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({
    extended: true
}));
router.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
//config email
let transport = nodemailer.createTransport({
    host: 'leafeon.rapidplex.com',
    port: 465,
    secure: true,
    auth: {
        user: 'donotreply@servicenodejs-jeffryazharirosman-2020.pw',
        pass: 'Rosman123123'
    }
});

// Default Route
// router.get('/', (reg, res) => {
//     return res.send({ status: 404, data: null, message: 'Page tidak tersedia' })
// });
// #routes:10 function untuk mengecek token
function isAuthenticated(req, res, next) {
    // var token = req.body.token || req.query.token || req.headers.authorization; // mengambil token di antara request
    var token = req.body.token; // mengambil token di antara request
    if (token) { //jika ada token

        jwt.verify(token, jwtKey, function (err, decoded) { //jwt melakukan verify
            if (err) { // apa bila ada error
                res.json({ message: 'Failed to authenticate token' }); // jwt melakukan respon
            } else { // apa bila tidak error
                req.decoded = decoded; // menyimpan decoded ke req.decoded
                next(); //melajutkan proses
            }
        });
    } else { // apa bila tidak ada token
        return res.status(403).send({ message: 'No token provided.' }); // melkukan respon kalau token tidak ada
    }
}

//check Log In
router.post('/member/login', function (req, res, next) {
    var email = req.body.data.email;
    var password = req.body.data.password;
    var reqToken = {
        email: email,
        password: password,
        date: new Date().getDate()
    }
    mysqlConnection.query(`SELECT * FROM member WHERE email = '${email}' AND password ='${password}'`, function (error, results, fields) {
        if (error) throw error;

        if (results.length == 0) {
            return res.send({ status: 401, data: null, message: 'Login gagal' });
        } else {
            const token = jwt.sign(reqToken, jwtKey);
            return res.send({ status: 200, data: results, token: token, message: 'Login berhasil' });
        }

    });
});


// daftar menjadi Member
router.post('/member/pra_daftar', function (req, res, next) {
    var nama_member = req.body.data.nama_member;
    var date_regis = req.body.data.date_regis;
    var email = req.body.data.email;
    var password = req.body.data.password;
    var status = req.body.data.status;
    mysqlConnection.query(`INSERT INTO pra_member (date_regis, nama_member, email, password, status) VALUES ('${date_regis}', '${nama_member}', '${email}', '${password}', '${status}')`, function (error, results) {
        if (error) throw error;
        const message = {
            from: 'donotreply@servicenodejs-jeffryazharirosman-2020.pw', // Sender address
            to: email,         // List of recipients
            subject: 'Verifikasi Email Makalah.ID', // Subject line
            html: `
                <table style="width:100%">
                    <tr>
                        <td>
                            <p>Yth Kawan Baca <span style="font-weight: bolder;">${nama_member}</span>,</p>
                            <p>Anda terdaftar sebagai pengguna aplikasi Makalah ID dengan Email: <span style="font-weight: bolder;">${email}</span>.</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>Silakan lakukan verifikasi untuk bisa login pada aplikasi Makalah ID. Copy Link di bawah ini dan buka pada browser Anda untuk melakukan verifikasi Email.</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span>LINK : <span><a href="https://servicenodejs-jeffryazharirosman-2020.pw/member/verifikasi/${email}/${date_regis}"  target="_blank" data-saferedirecturl="https://servicenodejs-jeffryazharirosman-2020.pw/member/verifikasi/${email}/${date_regis}">https://servicenodejs-jeffryazharirosman-2020.pw/member/verifikasi/${email}/${date_regis}<a></span></span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>Terima Kasih.</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>Salam, Baca</p>
                            <p>Makalah ID</p>
                        </td>
                    </tr>
                </table>
                ` // Plain text body
        };
        transport.sendMail(message, function (err, info) {
            if (err) {
                console.log("err", err)
            } else {
                console.log("Email telah terkirim:", info);
            }
        });
        res.status(201).send({ status: 201, message: 'Member baru berhasil ditambahkan.' });
    });
});

// re_send Verifikasi Email
router.post('/member/reSend_verifikasi', function (req, res, next) {
    var date_regis = req.body.data.date_regis;
    var email = req.body.data.email;
    mysqlConnection.query(`SELECT * FROM pra_member WHERE email = '${email}'`, function (error, results) {
        if (error) throw error;
        var nama_member_second;

        if (results.length == 0) {
            return res.send({ status: 400, data: null, message: 'Email tidak ditemukan' });
        } else {
            Object.keys(results).forEach((key) => {
                var row = results[key];
                nama_member_second = row.nama_member;
            });
            mysqlConnection.query(`UPDATE pra_member SET date_regis = '${date_regis}' WHERE pra_member.email = '${email}'`, function (error, results) {
                if (error) throw error;
                const message = {
                    from: 'donotreply@servicenodejs-jeffryazharirosman-2020.pw', // Sender address
                    to: email,         // List of recipients
                    subject: 'Verifikasi Email Makalah.ID', // Subject line
                    html: `
                <table style="width:100%">
                    <tr>
                        <td>
                            <p>Yth Kawan Baca <span style="font-weight: bolder;">${nama_member_second}</span>,</p>
                            <p>Anda terdaftar sebagai pengguna aplikasi Makalah ID dengan Email: <span style="font-weight: bolder;">${email}</span>.</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>Silakan lakukan verifikasi untuk bisa login pada aplikasi Makalah ID. Copy Link di bawah ini dan buka pada browser Anda untuk melakukan verifikasi Email.</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span>LINK : <span><a href="https://servicenodejs-jeffryazharirosman-2020.pw/member/verifikasi/${email}/${date_regis}"  target="_blank" data-saferedirecturl="https://servicenodejs-jeffryazharirosman-2020.pw/member/verifikasi/${email}/${date_regis}">https://servicenodejs-jeffryazharirosman-2020.pw/member/verifikasi/${email}/${date_regis}<a></span></span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>Terima Kasih.</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>Salam, Baca</p>
                            <p>Makalah ID</p>
                        </td>
                    </tr>
                </table>
                ` // Plain text body
                };
                transport.sendMail(message, function (err, info) {
                    if (err) {
                        console.log("err", err)
                    } else {
                        console.log("Email telah terkirim:", info);
                    }
                });
                res.status(200).send({ status: 200, message: 'Email Berhasil di Kirim.' });
            })
        }

    });
});

//verifikasi email
router.get('/member/verifikasi/:email/:date_regis', function (req, res, next) {
    var email = req.params.email;
    var date_regis = req.params.date_regis;
    mysqlConnection.query(`SELECT * FROM pra_member WHERE email = '${email}' AND date_regis = '${date_regis}'`, function (error, results, fields) {
        if (error) throw error;

        if (results.length == 0) {
            return res.sendFile(path.join(__dirname, '../', 'not_found.html'));
        } else {
            var nama_member_second;
            var email_second;
            var password_second;
            var status_second;

            Object.keys(results).forEach((key) => {
                var row = results[key];
                nama_member_second = row.nama_member;
                email_second = row.email;
                password_second = row.password;
                status_second = row.status;

            });
            console.log(nama_member_second, email_second, password_second, status_second);
            mysqlConnection.query(`INSERT INTO member (nama_member, email, password, status) VALUES ('${nama_member_second}', '${email_second}', '${password_second}', '${status_second}')`, function (error, results) {
                if (error) throw error;
                mysqlConnection.query(`DELETE FROM pra_member WHERE pra_member.email = '${email_second}'`, function (error, results) {
                    if (error) throw error;
                    const message = {
                        from: 'donotreply@servicenodejs-jeffryazharirosman-2020.pw', // Sender address
                        to: email,         // List of recipients
                        subject: 'Selamat Datang Kawan Baca Makalah.ID', // Subject line
                        html: `
                        <table style="width:100%">
                    <tr>
                        <td>
                            <p>Yth Kawan Baca <span style="font-weight: bolder;">${nama_member_second}</span>,</p>
                            <p>Anda terdaftar sebagai pengguna aplikasi Makalah ID dengan Email: <span style="font-weight: bolder;">${email_second}</span>.</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>Silakan login pada aplikasi Makalah ID dengan email Anda dan password yang Anda buat.</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>Terima Kasih.</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>Salam, Baca</p>
                            <p>Makalah ID</p>
                        </td>
                    </tr>
                </table>
                        ` // Plain text body
                    };
                    transport.sendMail(message, function (err, info) {
                        if (err) {
                            console.log("err", err)
                        } else {
                            console.log("Email telah terkirim:", info);
                        }
                    });
                    return res.sendFile(path.join(__dirname, '../', 'verifikasi.html'));
                })
            });
        }

    });
});

// check checking_pra_regis
router.post('/member/checking_pra_regis', function (req, res, next) {
    var email = req.body.data.email;
    mysqlConnection.query(`SELECT * FROM pra_member WHERE email = '${email}'`, function (error, results, fields) {
        if (error) throw error;

        if (results.length != 0) {
            return res.send({ status: 401, data: null, message: 'Checking gagal' });
        } else {
            return res.send({ status: 200, data: null, message: 'Checking berhasil' });
        }

    });
});

// check Log In
router.post('/member/checking_regis', function (req, res, next) {
    var email = req.body.data.email;
    mysqlConnection.query(`SELECT * FROM member WHERE email = '${email}'`, function (error, results, fields) {
        if (error) throw error;

        if (results.length != 0) {
            return res.send({ status: 401, data: null, message: 'Checking gagal' });
        } else {
            return res.send({ status: 200, data: null, message: 'Checking berhasil' });
        }

    });
});
//cheeck member
router.post('/member/checking_member', function (req, res, next) {
    var email = req.body.data.email;
    mysqlConnection.query(`SELECT * FROM member WHERE email = '${email}'`, function (error, results, fields) {
        if (error) throw error;

        if (results.length == 0) {
            return res.send({ status: 401, data: null, message: 'Checking gagal' });
        } else {
            return res.send({ status: 200, data: null, message: 'Checking berhasil' });
        }

    });
});
// insert forget password
router.post('/member/request_password', function (req, res, next) {
    var email = req.body.data.email;
    var date_request = req.body.data.date_request;
    mysqlConnection.query(`SELECT * FROM member WHERE email = '${email}'`, function (error, respons, fields) {
        if (error) throw error;
        var password;
        var nama;
        Object.keys(respons).forEach((key) => {
            var row = respons[key];
            password = row.password;
            nama = row.nama_member;

        })
        mysqlConnection.query(`INSERT INTO forget_password (id_lupa_password, email, date_request) VALUES (NULL, '${email}', '${date_request}')`, function (error, results, fields) {
            if (error) throw error;

            if (results) {
                const message = {
                    from: 'donotreply@servicenodejs-jeffryazharirosman-2020.pw', // Sender address
                    to: email,         // List of recipients
                    subject: 'Permohonan Lupa Password Makalah ID', // Subject line
                    html: `
                <div>
                    <div>
                        <p>Yth Kawan Baca <span style="font-weight: bolder;">${nama}</span>,</p>
                        <p>Anda terdaftar sebagai pengguna aplikasi Makalah ID dengan Email: <span style="font-weight: bolder;">${email}</span>.</p>
                    </div>
                    <div>
                        <p>Silakan login dengan mengunakan Email tersebut dan password : <span style="font-weight: bolder;">${password}</span></p>
                    </div>
                    <div>
                        <p>Terima Kasih.</p>
                    </div>
                    <br />
                    <div>
                        <p>Salam, Baca</p>
                        <p>Makalah ID</p>
                    </div>
                </div>
                ` // Plain text body
                };
                transport.sendMail(message, function (err, info) {
                    if (err) {
                        console.log("err", err)
                    } else {
                        console.log("Email telah terkirim:", info);
                    }
                });
                return res.send({ status: 201, data: null, message: 'berhasil' });
            } else {
                return res.send({ status: 401, data: null, message: 'gagal' });
            }

        });
    });

});
// Menampilkan data user detail
router.post('/member', isAuthenticated, function (req, res, next) {

    mysqlConnection.query('SELECT * FROM member', function (error, results, fields) {
        if (error) throw error;
        return res.send({ status: 200, data: results, message: 'List data' });
    });
});


// Menambahkan user baru 
// app.post('/user', function (req, res, next) {

//     let username = req.body.data.username;
//     let password = req.body.data.password;
//     let nama = req.body.data.nama;
//     let status = req.body.data.status;

//     if (!username) {
//         return res.status(400).send({ status: 400, data: null, message: 'Silakan isikan parameter username' });
//     } else if (!password) {
//         return res.status(400).send({ status: 400, data: null, message: 'Silakan isikan parameter password' });
//     } else if (!nama) {
//         return res.status(400).send({ status: 400, data: null, message: 'Silakan isikan parameter nama' });
//     } else if (!status || status == "") {
//         status = "Tidak diketahui"
//     }

//     dbConn.query("INSERT INTO administrator SET ? ", { username: username, password: password, nama: nama, status: status }, function (error, results, fields) {
//         if (error) throw error;
//         return res.status(201).send({ status: 201, message: 'User baru berhasil ditambahkan.' });
//     });
// });


//  Update detail user id
// app.put('/user', function (req, res, next) {

//     let user_id = req.body.user_id;
//     let user = req.body.user;
//     let name = req.body.name;
//     let email = req.body.email;

//     if (!user_id || !user) {
//         return res.status(400).send({ error: user, message: 'Silakan isikan parameter user dan user_id' });
//     }

//     dbConn.query("UPDATE users SET user = ?, name = ?, email = ? WHERE id = ?", [user, name, email, user_id], function (error, results, fields) {
//         if (error) throw error;
//         return res.send({ error: false, data: results, message: 'Data user berhasil diperbaharui.' });
//     });
// });


//  Delete user
// app.delete('/user', function (req, res, next) {

//     let user_id = req.body.user_id;

//     if (!user_id) {
//         return res.status(400).send({ error: true, message: 'Silakan isikan parameter user_id' });
//     }
//     dbConn.query('DELETE FROM users WHERE id = ?', [user_id], function (error, results, fields) {
//         if (error) throw error;
//         return res.send({ error: false, data: results, message: 'User berhasil dihapus.' });
//     });
// });


module.exports = router;
var express = require('express');
var path = require('path');
//bodyParser merubah data Mysql ke JSON
var bodyParser = require('body-parser');
const router = express.Router();
//import file connection DB
const mysqlConnection = require('../connect_db.js');
const image2base64 = require('image-to-base64');
const pdf2base64 = require('pdf-to-base64');

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({
    extended: true
}));
router.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});


// image2base64("src/file-cover-pdf/surat-riser-1.png") // you can also to use url
// pdf2base64("src/file-pdf/surat-riser.pdf") // you can also to use url
//     .then(
//         (response) => {
//             console.log(response); //cGF0aC90by9maWxlLmpwZw==
//         }
//     )
//     .catch(
//         (error) => {
//             console.log(error); //Exepection error....
//         }
//     );


router.post('/ads/new', function (req, res, next) {
    mysqlConnection.query(`SELECT * FROM banner ORDER BY id_banner ASC`, function (error, results, fields) {
        if (error) throw error;

        if (results.length == 0) {
            return res.send({ status: 200, data: [], message: 'List data kosong' });
        } else {
            onload(results);
        }
        async function onload(results) {
            var data = [];
            var string = JSON.stringify(results);
            var json = JSON.parse(string);

            for (var a = 0; a < json.length; a++) {
                var id_banner = json[a].id_banner;
                var nama_banner = json[a].nama_banner;
                var keterangan = json[a].keterangan;

                await image2base64("src/ads_banner/" + nama_banner).then((responsePng) => {
                    var prosesData = {
                        "id_banner": id_banner,
                        "nama_banner": nama_banner,
                        "keterangan": keterangan,
                        "base64Banner": "data:image/png;base64," + responsePng
                    };
                    data.push(prosesData);
                }).catch((error) => {
                    console.log(error); //Exepection error....
                    return res.send({ status: 404, data: null, message: 'File Error' });
                });
            }
            return res.send({ status: 200, data: data, message: 'List data', length: data.length });
        }
    });
});

// Menampilkan data produk detail
// router.get('/pruduct', function (req, res, next) {

//     mysqlConnection.query('SELECT * FROM makalah', function (error, results, fields) {
//         if (error) throw error;
//         return res.send({ status: 200, data: results, message: 'List data' });
//     });
// });

module.exports = router;